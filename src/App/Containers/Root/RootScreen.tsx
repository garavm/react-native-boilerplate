import React, { Component } from 'react';
import {View} from 'react-native';
import NavigationService from 'App/Services/NavigationService';
import styles from './RootScreenStyle';
import {connect} from 'react-redux';
import StartupActions from 'App/Stores/Startup/Actions';
import AppNavigator from 'App/Navigators/AppNavigator';
export interface Props {
  startup: any;
}
class RootScreen extends Component<Props> {
  componentDidMount() {
    // Run the startup saga when the application is starting
    this.props.startup();
  }

  render() {
    return (
      <View style={styles.container}>
        <AppNavigator ref={(navigatorRef: any) => NavigationService.setTopLevelNavigator(navigatorRef)}/>
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: (arg0: any) => void) => ({
  startup: () => dispatch(StartupActions.startup()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootScreen);
